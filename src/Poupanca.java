public class Poupanca extends Conta {
    @Override
    public void depositar(double valor) {
        setSaldo(getSaldo() + valor + valor * 0.02);

    }

    @Override
    public boolean sacar(double valor) {
        if(valor < getSaldo() || valor == getSaldo()){
            setSaldo(getSaldo() - valor);
            return true;
        }
        return false;

    }

    @Override
    double consultarSaldo() {
        return getSaldo();
    }
}
