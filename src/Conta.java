public abstract class Conta {
    private String numConta;
    private double saldo;



    abstract void depositar(double valor);
    abstract boolean sacar(double valor);
    abstract double consultarSaldo();

    protected void setSaldo(double saldo){
        this.saldo = saldo;
    }

    protected double getSaldo(){
        return this.saldo;
    }

    protected String getNumConta(){
        return this.numConta;
    }
    protected void setNumConta(String num){
        this.numConta = num;
    }
}
